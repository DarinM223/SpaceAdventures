#include "Game.h"
#include <iostream>
#include "exceptions.h"

Game::Game(std::string title, int width, int height)
    : width_(width), height_(height) {
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    throw sdl::InitException{};
  }

  this->window_ = {SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED,
                                    SDL_WINDOWPOS_UNDEFINED, width, height,
                                    SDL_WINDOW_SHOWN),
                   SDL_DestroyWindow};
  if (this->window_.get() == nullptr) {
    throw sdl::CreateWindowException{};
  }

  std::cout << "Width: " << this->width_ << " Height: " << this->height_
            << "\n";

  this->screen_ = SDL_GetWindowSurface(this->window_.get());
}

void Game::run() {
  UniqueSurfacePtr splash{SDL_LoadBMP("blah.bmp"), SDL_FreeSurface};
  if (splash.get() == nullptr) {
    throw sdl::LoadImageException{"blah.bmp"};
  }

  // Apply splash image onto screen.
  SDL_BlitSurface(splash.get(), nullptr, this->screen_, nullptr);

  SDL_UpdateWindowSurface(this->window_.get());

  SDL_Delay(2000);
}

Game::~Game() { SDL_Quit(); }
