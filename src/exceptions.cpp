#include "exceptions.h"

namespace sdl {

const char* InitException::what() const throw() {
  return "SDL could not initialize";
}

const char* CreateWindowException::what() const throw() {
  return "Error creating window";
}

const char* LoadImageException::what() const throw() {
  return "Error loading image";
}

}  // namespace sdl
