#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>
#include <string>

namespace sdl {

class InitException : public std::exception {
 public:
  const char* what() const throw() override;
};

class CreateWindowException : public std::exception {
 public:
  const char* what() const throw() override;
};

class LoadImageException : public std::exception {
 public:
  LoadImageException(std::string path) : path_(std::move(path)) {}
  const char* what() const throw() override;

 private:
  std::string path_;
};

}  // namespace sdl

#endif
