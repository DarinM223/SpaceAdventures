#include "Game.h"

int main(int argc, char *argv[]) {
  Game game{"New game", 640, 480};
  game.run();
  return 0;
}
