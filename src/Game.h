#ifndef GAME_H
#define GAME_H

#include <SDL.h>
#include <memory>
#include <string>

using UniqueWindowPtr =
    std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)>;
using UniqueSurfacePtr =
    std::unique_ptr<SDL_Surface, decltype(&SDL_FreeSurface)>;

class Game {
 public:
  Game(std::string title, int width, int height);
  void run();
  ~Game();

 private:
  int width_;
  int height_;
  UniqueWindowPtr window_{nullptr, nullptr};
  SDL_Surface *screen_;
};

#endif
